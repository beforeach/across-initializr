package across.initializr.application.config;

import across.initializr.application.domain.project.ProjectRequest;
import across.initializr.application.utils.Version;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public final class AcrossVersions {
    /**
     * Represents the versions for Across Platform
     */
    public static final Version PLATFORM_2_1_0 = Version.from("2.1.0.RELEASE");
    public static final Version PLATFORM_3_0_0 = Version.from("3.0.0.RELEASE");
    public static final Version PLATFORM_5_1_0 = Version.from("5.1.0.RELEASE");

    public static boolean isVersionBelow(Version version, ProjectRequest projectRequest) {
        return projectRequest.getPlatformVersion().compareTo(version) < 0;
    }
}
