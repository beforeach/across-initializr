package across.initializr.application.domain.generators.general;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
public class IntelliJRunConfigurationGenerator extends GenericModuleGenerator
{
	@Override
	public void buildModuleStructure( ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths ) {
		Map<String, Object> model = new LinkedHashMap<>( baseModel );
		model.put( "moduleName", getProjectRequest().getArtifact() );

		generator.processTemplate( "shared/DevRunConfiguration.xml", model,
		                           ".idea/runConfigurations/" + getProjectRequest().getApplicationClassName() + "_dev.xml" );
	}
}
