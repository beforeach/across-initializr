package across.initializr.application.domain.project;

import across.initializr.application.domain.generators.ModuleGenerator;
import across.initializr.application.domain.metadata.SpringBootConfigs;
import across.initializr.application.utils.Version;
import lombok.Data;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Arne Vandamme
 */
@Data
public class ProjectRequest {
    private final Collection<ModuleGenerator> moduleGenerators = new ArrayList<>();

    @Getter
    private Version platformVersion;
    private String group;
    private String artifact;
    private String applicationName;
    private String packageName;
    private String applicationClassPrefix;
    private Set<String> selectedModules = new HashSet<>();
    private Set<String> selectedOptions = new HashSet<>();
    private Set<SpringBootConfigs> selectedSpringBootConfigs = new HashSet<>();

    public String getApplicationClassName() {
        return applicationClassPrefix + "Application";
    }

    public String getApplicationResourcesKey() {
        return StringUtils.removeEnd(StringUtils.uncapitalize(getApplicationClassName()), "Application");
    }

    public boolean containsModule(String moduleId) {
        return moduleGenerators.stream()
                .anyMatch(g -> moduleId.equals(g.getModuleId()));
    }

    public boolean containsOption(String optionId) {
        return selectedOptions.contains(optionId);
    }

    public void addModuleGenerator(ModuleGenerator moduleGenerator) {
        moduleGenerators.add(moduleGenerator);
    }

    public boolean forAcross5OrHigher() {
        return getPlatformVersion().getMajor() > 4;
    }
}
