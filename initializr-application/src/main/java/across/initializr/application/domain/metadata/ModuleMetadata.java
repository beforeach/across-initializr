package across.initializr.application.domain.metadata;

import across.initializr.application.utils.VersionRange;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arne Vandamme
 */
@Data
public class ModuleMetadata
{
	private String id;
	private String name;
	private String description;
	private String groupId;
	private String artifactId;
	private String packageName;
	private String generator;
	private VersionRange versionRange = VersionRange.from( "2.0.1.RELEASE" );

	private List<LinkMetadata> links = new ArrayList<>();
	private List<OptionMetadata> options = new ArrayList<>();
	private List<String> dependencies = new ArrayList<>();
}
