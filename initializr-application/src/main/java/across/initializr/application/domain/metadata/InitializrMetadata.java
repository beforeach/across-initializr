package across.initializr.application.domain.metadata;

import across.initializr.application.utils.Version;
import across.initializr.application.utils.VersionRange;
import lombok.Getter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Arne Vandamme
 */
@Component
@Getter
@EnableConfigurationProperties(InitializrProperties.class)
public class InitializrMetadata
{
	private final List<ModuleGroup> moduleGroups;
	private final List<PresetMetadata> presets;
	private final List<Version> platformVersions;
	private final List<SpringBootConfigs> springBootConfigs;

	public InitializrMetadata( InitializrProperties properties ) {
		moduleGroups = properties.getModuleGroups();
		presets = properties.getPresets();
		platformVersions = properties.getPlatformVersions();
		springBootConfigs = properties.getSpringBootConfigs();
	}

	public ModuleMetadata getModuleMetadata( String moduleId ) {
		return moduleGroups.stream()
		                   .flatMap( moduleGroup -> moduleGroup.getModules().stream() )
		                   .filter( m -> m.getId().equals( moduleId ) )
		                   .findFirst()
		                   .orElse( null );
	}

	public PresetMetadata getPresetMetadata( String presetId ) {
		return presets.stream()
		              .filter( p -> p.getId().equals( presetId ) )
		              .findFirst()
		              .orElse( null );
	}

	public List<Version> versionsSince(VersionRange version) {
		return platformVersions.stream().filter(version::match).collect(Collectors.toList());
	}
}
