package across.initializr.application.domain.generators.modules;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;

/**
 * @author Arne Vandamme
 */
public class LoggingModuleGenerator extends GenericModuleGenerator
{
	@Override
	public void writeApplicationProperties( ApplicationProperties properties ) {
		if ( properties.isDefault() ) {
			properties.put( "logging.request-response.enabled", true );
		}
	}
}
