package across.initializr.application.domain.generators.general;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
public class FrontendResourcesGenerator extends GenericModuleGenerator
{
	@SneakyThrows
	@Override
	public void buildModuleStructure( ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths ) {
		File dir = generator.createDirectory( modulePaths.root( "src/main/frontend" ) );

		generator.unzip( "general/frontend/frontend-resources.zip", dir );

		String resourceKey = getProjectRequest().getApplicationResourcesKey();

		File gulpConfig = generator.createFile( modulePaths.root( "src/main/frontend/gulp/config.js" ) );
		String config = FileUtils.readFileToString( gulpConfig, "UTF-8" );
		Assert.notNull(config, "Cannot find gulpConfig file");
		config = StringUtils.replace( config, "const dest = root + \"\";", "const dest = '../resources/views/static/" + resourceKey + "/';" );
		config = StringUtils.replace( config, "foreach.be", "localhost" );
		config = StringUtils.replace( config, "foreach.local", "localhost" );
		FileUtils.writeStringToFile( gulpConfig, config, "UTF-8" );

		generator.createDirectory( modulePaths.resource( "views/static/" + resourceKey + "/js" ) );
		generator.copyResource( "general/frontend/project.scss", modulePaths.resource( "views/static/" + resourceKey + "/css/main.scss" ) );
	}

	@Override
	public void addReadMeSnippet( String section, List<String> snippets, ProjectGenerator generator, Map<String, Object> baseModel ) {
		if ( "frontend".equals( section ) ) {
			snippets.add( generator.renderTemplate( "general/frontend/README-frontend.md", baseModel ) );
		}
	}
}
