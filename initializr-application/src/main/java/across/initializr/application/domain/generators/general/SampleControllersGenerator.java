package across.initializr.application.domain.generators.general;

import across.initializr.application.config.AcrossVersions;
import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
public class SampleControllersGenerator extends GenericModuleGenerator {

    private static final String VIEWS_STATIC = "views/static/";

    @Override
    public void addApplicationClassImports(Collection<String> imports) {
        if (isBlogEntitiesIncluded()) {
            imports.add( "org.springframework.data.web.config.EnableSpringDataWebSupport" );
        }
    }

    @Override
    public void addApplicationClassAnnotations(Collection<String> annotations) {
        annotations.add( "@Import({ DataSourceAutoConfiguration.class, H2ConsoleAutoConfiguration.class })" );
        if (isBlogEntitiesIncluded()) {
            annotations.add( "@EnableSpringDataWebSupport" );
        }
    }

    @Override
    public void buildModuleStructure(ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths) {
        ProjectRequest projectRequest = getProjectRequest();

        Map<String, Object> model = new LinkedHashMap<>( baseModel );
        model.put( "frontendResourcesIncluded", projectRequest.containsModule( "general-frontend-resources" ) );
        model.put( "messageSourceIncluded", projectRequest.containsModule( "general-message-source" ) );
        model.put( "sampleEntitiesIncluded", isBlogEntitiesIncluded() );

        generator.processTemplate( "general/sample-controllers/DefaultLayoutTemplate.java", model,
                modulePaths.code( "web/ui/DefaultLayoutTemplate.java" ) );

        if (isVersionBelow(AcrossVersions.PLATFORM_2_1_0)) {
            generator.processTemplate("general/sample-controllers/ExamplePagesController.java", model,
                    modulePaths.code("web/ExamplePagesController.java"));
        } else {
            generator.processTemplate("ax-2-1-0/general/sample-controllers/ExamplePagesController.java", model,
                    modulePaths.code("web/ExamplePagesController.java"));
        }

        String views = "views/th/" + projectRequest.getApplicationResourcesKey() + "/";

        if (isBlogEntitiesIncluded()) {
            if ((AcrossVersions.isVersionBelow(AcrossVersions.PLATFORM_3_0_0, projectRequest))) {
                generator.processTemplate("general/sample-controllers/BlogController.java", model,
                        modulePaths.code("web/BlogController.java"));
            } else {
                generator.processTemplate("ax-3-0-0/general/sample-controllers/BlogController.java", model,
                        modulePaths.code("web/BlogController.java"));
            }

            generator.processTemplate( "general/sample-controllers/page-blog-post.thtml", model,
                    modulePaths.resource( views + "blog-post.html" ) );
        }

        generator.processTemplate( "general/sample-controllers/default-layout.thtml", model,
                modulePaths.resource( views + "layout/default-layout.html" ) );
        generator.processTemplate( "general/sample-controllers/page-homepage.thtml", model,
                modulePaths.resource( views + "homepage.html" ) );
        generator.processTemplate( "general/sample-controllers/page-about.thtml", model,
                modulePaths.resource( views + "about.html" ) );
        generator.processTemplate( "general/sample-controllers/page-no-layout.thtml", model,
                modulePaths.resource( views + "no-layout.html" ) );

        String resourceKey = projectRequest.getApplicationResourcesKey();
        generator.copyResource( "general/sample-controllers/home-bg.jpg",
                modulePaths.resource(VIEWS_STATIC + resourceKey + "/img/home-bg.jpg"));
        generator.copyResource( "general/sample-controllers/about-bg.jpg",
                modulePaths.resource(VIEWS_STATIC + resourceKey + "/img/about-bg.jpg"));

        if (!projectRequest.containsModule( "general-frontend-resources" )) {
            generator.copyResource(
                    "general/sample-controllers/main.js",
                    modulePaths.resource(VIEWS_STATIC + resourceKey + "/js/main.js")
            );
            generator.copyResource(
                    "general/sample-controllers/main.css",
                    modulePaths.resource(VIEWS_STATIC + resourceKey + "/css/main.css")
            );
        }
    }

    private boolean isBlogEntitiesIncluded() {
        ProjectRequest projectRequest = getProjectRequest();
        return projectRequest.containsOption( "ahm-sample-entity" );
    }
}
