package across.initializr.application.domain.generators;

import across.initializr.application.domain.metadata.Dependency;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
public interface ModuleGenerator
{
	String getModuleId();

	void addDependency( Collection<Dependency> dependencies, boolean dependencyManagementSection );

	void addApplicationClassImports( Collection<String> imports );

	void addAcrossApplicationModules( Collection<String> modules );

	void writeApplicationProperties( ApplicationProperties properties );

	void addApplicationClassAnnotations( Collection<String> annotations );

	void addApplicationClassCode( Collection<String> codeSnippets, ProjectGenerator generator, Map<String, Object> baseModel );

	void buildModuleStructure( ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths );

	void setMetadata( across.initializr.application.domain.metadata.InitializrMetadata metadata );

	void setModuleMetadata( across.initializr.application.domain.metadata.ModuleMetadata moduleMetadata );

	void setProjectRequest( ProjectRequest projectRequest );

	void addReadMeSnippet( String section, List<String> snippets, ProjectGenerator generator, Map<String, Object> baseModel );
}
