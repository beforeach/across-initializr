package across.initializr.application.domain.metadata;

import across.initializr.application.domain.project.ProjectGenerator;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arne Vandamme
 */
@Data
public class PresetMetadata
{
	private String id;
	private String name;
	private String description;
    private Class<ProjectGenerator> generator;
	private List<String> modules = new ArrayList<>();
	private List<String> options = new ArrayList<>();
}
