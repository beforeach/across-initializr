package across.initializr.application.domain.generators.modules;

import across.initializr.application.config.AcrossVersions;
import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author Arne Vandamme
 */
public class AdminWebModuleGenerator extends GenericModuleGenerator {
    @Override
    public void writeApplicationProperties(ApplicationProperties properties) {
        if (properties.isDefault()) {
            properties.put("adminWebModule.login.rememberMe.key", UUID.randomUUID());
            properties.put("adminWebModule.root-path", "/admin");

            if (!getProjectRequest().containsModule( "application-info-module" )) {
                properties.put( "adminWebModule.title", getProjectRequest().getApplicationName() );
            }
        }
    }

    @Override
    public void buildModuleStructure(ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths) {
        ProjectRequest projectRequest = getProjectRequest();
        if (projectRequest.containsOption( "awm-admin-controller" )) {
            if (isVersionBelow(AcrossVersions.PLATFORM_2_1_0)) {
				generator.processTemplate("modules/admin-web-module/DemoAdminController.java", baseModel,
						modulePaths.code("web/admin/DemoAdminController.java"));
			} else {
				generator.processTemplate("ax-2-1-0/modules/admin-web-module/DemoAdminController.java", baseModel,
						modulePaths.code("web/admin/DemoAdminController.java"));
			}

            generator.copyResource(
                    "modules/admin-web-module/demo.thtml",
                    modulePaths.resource( "views/th/" + projectRequest.getApplicationResourcesKey() + "/admin/demo.html" )
            );
        }
        if (!projectRequest.containsModule( "user-module" )) {
            // Register default user configuration
            generator.processTemplate( "modules/admin-web-module/AdminWebUserConfiguration.java", baseModel,
                    modulePaths.code( "extensions/AdminWebUserConfiguration.java" ) );
        }
    }

    @Override
    public void addReadMeSnippet(String section, List<String> snippets, ProjectGenerator generator, Map<String, Object> baseModel) {
        if ("features".equals( section )) {
            snippets.add( generator.renderTemplate( "modules/admin-web-module/README-features.md", baseModel ) );
        }
    }
}
