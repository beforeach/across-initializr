package across.initializr.application.domain.project;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.generators.ModuleGenerator;
import across.initializr.application.domain.metadata.InitializrMetadata;
import across.initializr.application.domain.metadata.ModuleMetadata;
import across.initializr.application.domain.metadata.SpringBootConfigs;
import liquibase.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Arne Vandamme
 */
@Component
@RequiredArgsConstructor
public class ProjectRequestBuilder {
    private final AutowireCapableBeanFactory beanFactory;
    private final InitializrMetadata metadata;

    public ProjectRequest buildProjectRequest(ProjectRequestForm requestForm) {
        ProjectRequest projectRequest = new ProjectRequest();
        BeanUtils.copyProperties(requestForm, projectRequest);

        projectRequest.getSelectedModules()
                .forEach(moduleId -> {
                    ModuleMetadata moduleMetadata = metadata.getModuleMetadata(moduleId);

                    ModuleGenerator moduleGenerator = createModuleGenerator(moduleMetadata);
                    moduleGenerator.setMetadata(metadata);
                    moduleGenerator.setModuleMetadata(moduleMetadata);
                    moduleGenerator.setProjectRequest(projectRequest);

                    projectRequest.addModuleGenerator(moduleGenerator);
                });

        Set<SpringBootConfigs> items = metadata.getSpringBootConfigs().stream()
                .filter(i -> requestForm.getSelectedSpringBootConfigs()
                        .contains(i.getId()))
                .collect(Collectors.toSet());
        projectRequest.setSelectedSpringBootConfigs(items);

        return projectRequest;
    }

    @SuppressWarnings("all")
    @SneakyThrows
    private ModuleGenerator createModuleGenerator(ModuleMetadata moduleMetadata) {
        if (!StringUtils.isEmpty(moduleMetadata.getGenerator())) {
            return beanFactory.createBean((Class<ModuleGenerator>) Class.forName(moduleMetadata.getGenerator()));
        }

        return new GenericModuleGenerator();
    }
}
