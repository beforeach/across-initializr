package across.initializr.application.domain.metadata;

import lombok.Data;

/**
 * @author Arne Vandamme
 */
@Data
public class OptionMetadata
{
	private String id;
	private String name;
	private String description;
}
