package across.initializr.application.domain.generators.foreach;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ProjectGenerator;

import java.util.Collection;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
public class ImageServerCoreGenerator extends GenericModuleGenerator
{
	@Override
	public void addApplicationClassImports( Collection<String> imports ) {
		imports.add( "org.springframework.context.annotation.Bean" );
		imports.add( "com.foreach.imageserver.core.ImageServerCoreModule" );
	}

	@Override
	public void addApplicationClassCode( Collection<String> codeSnippets, ProjectGenerator generator, Map<String, Object> baseModel ) {
		codeSnippets.add( "@Bean\n" +
				                  "\tpublic ImageServerCoreModule imageServerCoreModule() {\n" +
				                  "\t\treturn new ImageServerCoreModule();\n" +
				                  "\t}" );
	}

	@Override
	public void addAcrossApplicationModules( Collection<String> modules ) {
	}

	@Override
	public void writeApplicationProperties( ApplicationProperties properties ) {
		if ( properties.isDefault() ) {
			properties.put( "imageServerCore.rootPath", "/resources/images" );
			properties.put( "imageServerCore.accessToken", getProjectRequest().getApplicationResourcesKey() + "-imgserver-changeme" );
			properties.put( "imageServerCore.createLocalClient", true );
			properties.put( "imageServerCore.md5HashToken", getProjectRequest().getApplicationResourcesKey() + "-hash-imgserver-changeme" );
			properties.put( "imageServerCore.transformers.imageMagick.enabled", true );
			properties.put( "imageServerCore.transformers.imageMagick.useGraphicsMagick", true );
		}

		if ( properties.isProfile( "dev" ) ) {
			properties.put( "imageServerCore.imageServerUrl", "http://localhost:${server.port}/resources/images" );
			properties.put( "imageServerCore.store.folder", "../local-data/images" );
			properties.put( "imageServerCore.streaming.provideStackTrace", true );
			properties.put(
					"imageServerCore.transformers.imageMagick.# Preferably add the location of GraphicsMagick ('path' property) to the local application config",
					null
			);
			properties.put( "imageServerCore.transformers.imageMagick.#path", "/usr/local/bin" );
		}

		if ( properties.isProfile( "prod" ) ) {
			properties.put( "imageServerCore.transformers.imageMagick.# win - path", "c:/Program Files/GraphicsMagick-1.3.19-Q8" );
			properties.put( "imageServerCore.transformers.imageMagick.# osx - path", "/usr/local/bin" );
		}
	}
}
