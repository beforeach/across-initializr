package across.initializr.application.domain.project;

import across.initializr.application.utils.Version;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Arne Vandamme
 */
@Data
public class ProjectRequestForm
{
	private Version platformVersion;
	private String presetId = "single-blank";
	private String group = "com.example";
	private String artifact = "demo";
	private String applicationName = "Demo";
	private String applicationClassPrefix = "Demo";
	private String packageName = "com.example.demo";
	private Set<String> selectedModules = new HashSet<>();
	private Set<String> selectedOptions = new HashSet<>();
	private Set<String> selectedSpringBootConfigs = new HashSet<>();
}
