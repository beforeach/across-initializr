package across.initializr.application.domain.generators.general;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;

import java.util.Map;

/**
 * @author Arne Vandamme
 */
public class MessageSourceGenerator extends GenericModuleGenerator
{
	@Override
	public void buildModuleStructure( ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths ) {
		generator.copyResource(
				"shared/messages.properties",
				modulePaths.resource( "messages/" + getProjectRequest().getApplicationResourcesKey() + "/default.properties" )
		);
	}
}
