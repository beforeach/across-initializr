package across.initializr.application.domain.generators.modules;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.metadata.Dependency;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Raf Ceuls.
 */
public class WebCmsModuleGenerator extends GenericModuleGenerator
{
	private static final String CLOUDINARY_VERSION = "1.14.0";

	@Override
	public void writeApplicationProperties( ApplicationProperties properties ) {
		ProjectRequest projectRequest = getProjectRequest();

		if ( !projectRequest.containsModule( "image-server-core-module" ) ) {
			if ( projectRequest.containsOption( "wcm-imageserver" ) ) {
				if ( properties.isDefault() ) {
					properties.put( "webCmsModule.images.imageServer.enabled", true );
				}
				else {
					properties.put( "webCmsModule.images.imageServer.url", "http://imageserver.domain/resources/images" );
					properties.put( "webCmsModule.images.imageServer.hashToken", "changeme-optional-hashToken" );
					properties.put( "webCmsModule.images.imageServer.accessToken", "changeme-accessToken" );
				}
			}

			if ( projectRequest.containsOption( "wcm-cloudinary" ) ) {
				if ( properties.isDefault() ) {
					properties.put( "webCmsModule.images.cloudinary.enabled", true );
				}
				else {
					properties.put( "webCmsModule.images.cloudinary.cloudName", "changeme-cloudName" );
					properties.put( "webCmsModule.images.cloudinary.apiKey", "changeme-apiKey" );
					properties.put( "webCmsModule.images.cloudinary.apiSecret", "changeme-apiSecret" );
				}
			}
		}
	}

	@Override
	public void addDependency( Collection<Dependency> dependencies, boolean dependencyManagementSection ) {
		super.addDependency( dependencies, dependencyManagementSection );

		ProjectRequest projectRequest = getProjectRequest();

		if ( !projectRequest.containsModule( "image-server-core-module" ) ) {
			if ( dependencyManagementSection ) {
				if ( projectRequest.containsOption( "wcm-cloudinary" ) ) {
					dependencies.add( Dependency.builder().groupId( "com.cloudinary" ).artifactId( "cloudinary-http44" ).version( CLOUDINARY_VERSION )
					                            .build() );
				}
			}
			else {
				if ( projectRequest.containsOption( "wcm-cloudinary" ) ) {
					dependencies.add( Dependency.builder().groupId( "com.cloudinary" ).artifactId( "cloudinary-http44" ).build() );
				}
				else if ( projectRequest.containsOption( "wcm-imageserver" ) ) {
					dependencies.add( Dependency.builder().groupId( "com.foreach.imageserver" ).artifactId( "imageserver-client" ).build() );
				}
			}
		}
	}

	@Override
	public void buildModuleStructure( ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths ) {
		ProjectRequest projectRequest = getProjectRequest();
		Map<String, Object> model = new LinkedHashMap<>( baseModel );
		model.put( "includeSampleEntity", projectRequest.containsOption( "wcm-samples" ) );

		if ( projectRequest.containsOption( "wcm-samples" ) ) {
			generator.processTemplate( "modules/webcms-module/WebCmsDefaultAssetsInstaller.java", model,
			                           modulePaths.code( "installers/WebCmsDefaultAssetsInstaller.java" ) );

			generator.copyResource( "modules/webcms-module/base-content.yml", modulePaths.resource( "installers/base-content.yml" ) );
		}
	}
}
