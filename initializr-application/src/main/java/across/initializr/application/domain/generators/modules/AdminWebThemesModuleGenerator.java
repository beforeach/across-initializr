package across.initializr.application.domain.generators.modules;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;

/**
 * @author Steven Gentens
 */
public class AdminWebThemesModuleGenerator extends GenericModuleGenerator {
    @Override
    public void writeApplicationProperties(ApplicationProperties properties) {
        if (getProjectRequest().forAcross5OrHigher()) {
            if (properties.isDefault()) {
                properties.put("adminWebModule.theme", "classic");
            }
        }
    }
}
