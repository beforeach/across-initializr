package across.initializr.application.domain.metadata;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arne Vandamme
 */
@Data
public class ModuleGroup
{
	private String name;

	private List<ModuleMetadata> modules = new ArrayList<>();
}
