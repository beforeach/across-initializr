package across.initializr.application.domain.generators.modules;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;

/**
 * Specific ModuleGenerator for the FMM
 */
public class FileManagerModuleGenerator extends GenericModuleGenerator {
    @Override
    public void writeApplicationProperties(ApplicationProperties properties) {
        if (properties.isProfile("dev")) {
            properties.put("fileManagerModule.localRepositoriesRoot", "local-data/storage");
        }
    }
}
