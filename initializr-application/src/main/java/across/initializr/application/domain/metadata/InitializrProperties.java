package across.initializr.application.domain.metadata;

import across.initializr.application.utils.Version;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arne Vandamme
 */
@Data
@ConfigurationProperties(prefix = "initializr")
public class InitializrProperties
{
	private URI informationManagerEndpoint;
	private List<PresetMetadata> presets = new ArrayList<>();
	private List<Version> platformVersions = new ArrayList<>();
	private List<ModuleGroup> moduleGroups = new ArrayList<>();
	private List<SpringBootConfigs> springBootConfigs = new ArrayList<>();
}
