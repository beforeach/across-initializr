package across.initializr.application.domain.metadata;

import lombok.Data;

/**
 * @author Arne Vandamme
 */
@Data
public class LinkMetadata
{
	private String rel;
	private String href;
	private String description;
}
