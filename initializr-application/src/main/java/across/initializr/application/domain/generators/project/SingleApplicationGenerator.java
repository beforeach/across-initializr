package across.initializr.application.domain.generators.project;

import across.initializr.application.domain.metadata.Dependency;
import across.initializr.application.domain.metadata.SpringBootConfigs;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;
import across.initializr.application.utils.VersionParser;
import across.initializr.application.utils.VersionRange;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Generates a single application module.
 *
 * @author Arne Vandamme
 */
public class SingleApplicationGenerator extends ProjectGenerator {
    private static final VersionRange autoconfigureVersionRange = VersionParser.DEFAULT.parseRange("2.1.0.RELEASE");
    private static final String JAVA_EXTENSION = ".java";

    @Override
    protected void generateProjectStructure(ProjectRequest projectRequest) {
        super.generateProjectStructure(projectRequest);

        Map<String, Object> baseModel = resolveBaseModel(projectRequest);

        ModulePaths modulePaths = writeDirectoryStructure(projectRequest);
        writeReadMe(projectRequest, baseModel);
        writeMavenGradleFiles(projectRequest, baseModel);
        writeProperties(projectRequest, modulePaths);
        writeApplicationClassAndIntegrationTest(projectRequest, baseModel, modulePaths);

        projectRequest.getModuleGenerators().forEach(moduleGenerator -> moduleGenerator.buildModuleStructure(this, baseModel, modulePaths));
    }

    private void writeReadMe(ProjectRequest projectRequest, Map<String, Object> baseModel) {
        Map<String, Object> model = new LinkedHashMap<>(baseModel);
        model.put("applicationName", projectRequest.getApplicationName());
        model.put("applicationClass", projectRequest.getApplicationClassName());
        model.put("resourcesKey", projectRequest.getApplicationResourcesKey());

        List<String> featureSnippets = new ArrayList<>();
        projectRequest.getModuleGenerators().forEach(
                moduleGenerator -> moduleGenerator.addReadMeSnippet("features", featureSnippets, this, Collections.unmodifiableMap(model))
        );
        model.put("features", featureSnippets);

        List<String> frontendSnippets = new ArrayList<>();
        projectRequest.getModuleGenerators().forEach(
                moduleGenerator -> moduleGenerator.addReadMeSnippet("frontend", frontendSnippets, this, Collections.unmodifiableMap(model))
        );
        model.put("frontend", frontendSnippets);

        processTemplate("shared/README.md", model, "README.md");
    }

    private void writeProperties(ProjectRequest projectRequest, ModulePaths modulePaths) {
        if (projectRequest.getPlatformVersion().getMajor() < 5) {
            copyResource("shared/build.properties", modulePaths.resource("build.properties"));
        }

        ApplicationProperties defaultProps = new ApplicationProperties("");
        projectRequest.getModuleGenerators().forEach(moduleGenerator -> moduleGenerator.writeApplicationProperties(defaultProps));

        if (projectRequest.forAcross5OrHigher()) {
            defaultProps.put("build.number", wrapPropertyAsString("@build.revision@"));
            defaultProps.put("applicationInfo.buildId", wrapPropertyAsString("@pom.version@-@build.revision@"));
            defaultProps.put("applicationInfo.buildDate", wrapPropertyAsString("@timestamp@"));
        }

        writeText(createFile(modulePaths.resource("application.yml")), defaultProps.toYaml());

        ApplicationProperties devProps = new ApplicationProperties("dev");
        devProps.put("server.port", 8080);
        devProps.put("server.error.includeStacktrace", "ALWAYS");
        devProps.put("across.development.active", true);
        devProps.put("spring.datasource.url", "jdbc:h2:./local-data/db/" + projectRequest.getApplicationResourcesKey() + "-db");
        devProps.put("spring.datasource.username", "sa");

        if (projectRequest.forAcross5OrHigher()) {
            devProps.put("applicationInfo.buildDate", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()));
        }


        projectRequest.getModuleGenerators().forEach(moduleGenerator -> moduleGenerator.writeApplicationProperties(devProps));
        writeText(createFile(modulePaths.resource("application-dev.yml")), devProps.toYaml());

        ApplicationProperties prodProps = new ApplicationProperties("prod");
        prodProps.put("spring.h2.console.enabled", false);
        prodProps.put("server.port", 8080);
        prodProps.put("server.use-forward-headers", true);
        projectRequest.getModuleGenerators().forEach(moduleGenerator -> moduleGenerator.writeApplicationProperties(prodProps));
        writeText(createFile(modulePaths.resource("application-prod.yml")), prodProps.toYaml());
    }

    /**
     * Wraps the given value in {@code '} so that it is recognized as a string.
     */
    private String wrapPropertyAsString(String toWrap) {
        return StringUtils.wrap(toWrap, "\"");
    }

    private void writeApplicationClassAndIntegrationTest(ProjectRequest projectRequest, Map<String, Object> baseModel, ModulePaths modulePaths) {
        Map<String, Object> model = new LinkedHashMap<>(baseModel);
        model.put("className", projectRequest.getApplicationClassName());

        Collection<String> imports = new LinkedHashSet<>();
        projectRequest.getModuleGenerators().forEach(generator -> generator.addApplicationClassImports(imports));
        model.put("imports", imports);

        Collection<String> moduleNames = new LinkedHashSet<>();
        projectRequest.getModuleGenerators().forEach(generator -> generator.addAcrossApplicationModules(moduleNames));
        model.put("moduleNames", StringUtils.join(moduleNames, ",\n\t\t\t\t"));

        Collection<String> applicationAnnotations = new LinkedHashSet<>();
        projectRequest.getModuleGenerators().forEach(generator -> generator.addApplicationClassAnnotations(applicationAnnotations));

        if (projectRequest.getPlatformVersion().compareTo(autoconfigureVersionRange.getLowerVersion()) < 0) {
            model.put("applicationAnnotations", applicationAnnotations);
        }

        Collection<String> codeSnippets = new LinkedHashSet<>();
        projectRequest.getModuleGenerators().forEach(generator -> generator.addApplicationClassCode(codeSnippets, this, baseModel));
        model.put("codeSnippets", codeSnippets);

        if (projectRequest.forAcross5OrHigher()) {
            processTemplate("shared/Application.java", model, modulePaths.code("../" + projectRequest.getApplicationClassName() + JAVA_EXTENSION));
        } else {
            processTemplate("shared/Application-pre-ax-5.java", model, modulePaths.code("../" + projectRequest.getApplicationClassName() + JAVA_EXTENSION));
        }
        processTemplate(
                "shared/ITApplication.java",
                baseModel,
                modulePaths.integrationTest("IT" + projectRequest.getApplicationClassName() + JAVA_EXTENSION)
        );
    }

    private void writeMavenGradleFiles(ProjectRequest projectRequest, Map<String, Object> baseModel) {
        Map<String, Object> model = new LinkedHashMap<>(baseModel);
        model.put("groupId", projectRequest.getGroup());
        model.put("artifactId", projectRequest.getArtifact());
        model.put("version", "1.0.0-SNAPSHOT");
        model.put("packaging", "jar");
        model.put("name", projectRequest.getApplicationName());
        model.put("description", projectRequest.getApplicationName());
        model.put("mainClass", projectRequest.getPackageName() + "." + projectRequest.getApplicationClassName());

        Set<Dependency> dependencyManagement = new LinkedHashSet<>();
        projectRequest.getModuleGenerators().forEach(moduleGenerator -> moduleGenerator.addDependency(dependencyManagement, true));
        model.put("dependencyManagement", dependencyManagement);

        Set<Dependency> dependencyList = new LinkedHashSet<>();
        dependencyList.add(Dependency.builder().groupId("com.foreach.across").artifactId("across-web").build());
        projectRequest.getModuleGenerators().forEach(moduleGenerator -> moduleGenerator.addDependency(dependencyList, false));

        projectRequest.getSelectedSpringBootConfigs().forEach(springBootStarter ->
                dependencyList.add(Dependency.builder().groupId(SpringBootConfigs.GROUP_ID).artifactId(springBootStarter.getId()).build())
        );

        String applicationPomMaven = "starter-pom-pre-ax-5.xml";
        if (projectRequest.forAcross5OrHigher()) {
            applicationPomMaven = "starter-pom-application.xml";
        }

        model.put("dependencies", dependencyList);

        processTemplate("projects/single-application/" + applicationPomMaven, model, "pom.xml");
        if (!projectRequest.forAcross5OrHigher()) {
            processTemplate("projects/single-application/starter-build-gradle-pre-ax-5.mustache", model, "build.gradle");
        }

    }

    @SneakyThrows
    private ModulePaths writeDirectoryStructure(ProjectRequest projectRequest) {
        ModulePaths modulePaths = new ModulePaths();

        String packageLocation = projectRequest.getPackageName().replace(".", "/") + "/application";
        File src = new File(createFile("src/main/java"), packageLocation);
        FileUtils.forceMkdir(src);
        modulePaths.setCode(Paths.get("src/main/java", packageLocation).toString());

        File res = new File(createFile("src/main"), "resources");
        FileUtils.forceMkdir(res);
        modulePaths.setResources("src/main/resources");
        modulePaths.setTestResources("src/test/resources");

        File test = new File(createFile("src/test/java"), packageLocation);
        FileUtils.forceMkdir(test);
        modulePaths.setTest(Paths.get("src/test/java", packageLocation).toString());

        modulePaths.setIntegrationTest("src/test/java/it");

        return modulePaths;
    }

    private Map<String, Object> resolveBaseModel(ProjectRequest projectRequest) {
        Map<String, Object> model = new LinkedHashMap<>();
        model.put("includeAutoconfigueDependency", autoconfigureVersionRange.match(projectRequest.getPlatformVersion()));
        model.put("platformVersion", projectRequest.getPlatformVersion());
        model.put("packageName", projectRequest.getPackageName());
        model.put("modulePackage", projectRequest.getPackageName() + ".application");
        model.put("moduleResources", projectRequest.getApplicationResourcesKey());
        model.put("applicationClass", projectRequest.getApplicationClassName());
        return model;
    }
}
