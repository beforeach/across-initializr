package across.initializr.application.domain.metadata;

import across.initializr.application.utils.VersionRange;
import lombok.Data;

/**
 * @author Marc Vanbrabant
 */
@Data
public class SpringBootConfigs {
    public static final String GROUP_ID = "org.springframework.boot";

    private String id;
    private String name;
    private String description;
    private VersionRange versionRange;
}
