package across.initializr.application.domain.project;

import java.nio.file.Paths;

/**
 * @author Arne Vandamme
 */
public class ModulePaths
{
	private String root = "", code, test, integrationTest, resources, testResources;

	public void setRoot( String root ) {
		this.root = root;
	}

	public void setCode( String code ) {
		this.code = code;
	}

	public void setTest( String test ) {
		this.test = test;
	}

	public void setIntegrationTest( String integrationTest ) {
		this.integrationTest = integrationTest;
	}

	public void setResources( String resources ) {
		this.resources = resources;
	}

	public void setTestResources( String testResources ) {
		this.testResources = testResources;
	}

	public String root( String path ) {
		return Paths.get( root, path ).toString();
	}

	public String code( String path ) {
		return Paths.get( code, path ).toString();
	}

	public String test( String path ) {
		return Paths.get( test, path ).toString();
	}

	public String integrationTest( String path ) {
		return Paths.get( integrationTest, path ).toString();
	}

	public String resource( String path ) {
		return Paths.get( resources, path ).toString();
	}

	public String testResource( String path ) {
		return Paths.get( testResources, path ).toString();
	}
}
