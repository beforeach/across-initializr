package across.initializr.application.domain.metadata;

import lombok.Builder;
import lombok.Data;

/**
 * @author Arne Vandamme
 */
@Data
@Builder
public class Dependency
{
	private String groupId;
	private String artifactId;
	private String version;
	private String scope;
}
