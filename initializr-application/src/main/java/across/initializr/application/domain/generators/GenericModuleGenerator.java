package across.initializr.application.domain.generators;

import across.initializr.application.config.AcrossVersions;
import across.initializr.application.domain.metadata.Dependency;
import across.initializr.application.domain.metadata.InitializrMetadata;
import across.initializr.application.domain.metadata.ModuleMetadata;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;
import across.initializr.application.utils.Version;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
@Getter
@Setter
@RequiredArgsConstructor
public class GenericModuleGenerator implements ModuleGenerator
{
	private InitializrMetadata metadata;
	private ModuleMetadata moduleMetadata;
	private ProjectRequest projectRequest;

	@Override
	public final String getModuleId() {
		return moduleMetadata.getId();
	}

	@Override
	public void addDependency( Collection<Dependency> dependencies, boolean dependencyManagementSection ) {
		if ( !dependencyManagementSection && !StringUtils.isEmpty( moduleMetadata.getGroupId() ) ) {
			dependencies.add( Dependency.builder().groupId( moduleMetadata.getGroupId() ).artifactId( moduleMetadata.getArtifactId() ).build() );
		}
	}

	@Override
	public void addApplicationClassImports( Collection<String> imports ) {
		if ( !StringUtils.isEmpty( moduleMetadata.getPackageName() ) ) {
			imports.add( moduleMetadata.getPackageName() + "." + moduleMetadata.getName() );
		}
	}

	@Override
	public void addAcrossApplicationModules( Collection<String> modules ) {
		if ( !StringUtils.isEmpty( moduleMetadata.getPackageName() ) ) {
			modules.add( moduleMetadata.getName() + ".NAME" );
		}
	}

	@Override
	public void addApplicationClassAnnotations( Collection<String> annotations ) {

	}

	@Override
	public void addApplicationClassCode( Collection<String> codeSnippets, ProjectGenerator generator, Map<String, Object> baseModel ) {
	}

	@Override
	public void writeApplicationProperties( ApplicationProperties properties ) {
	}

	@Override
	public void buildModuleStructure( ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths ) {

	}

	@Override
	public void addReadMeSnippet( String section, List<String> snippets, ProjectGenerator generator, Map<String, Object> baseModel ) {

	}

	protected boolean isVersionBelow(Version version) {
		return AcrossVersions.isVersionBelow(version, projectRequest);
	}
}
