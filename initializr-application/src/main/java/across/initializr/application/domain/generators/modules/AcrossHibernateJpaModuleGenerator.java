package across.initializr.application.domain.generators.modules;

import across.initializr.application.config.AcrossVersions;
import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ModulePaths;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
public class AcrossHibernateJpaModuleGenerator extends GenericModuleGenerator
{
	@Override
	public void writeApplicationProperties( ApplicationProperties properties ) {
		if ( properties.isDefault() ) {
			properties.put( "acrossHibernate.create-unit-of-work-factory", true );
		}

		if ( properties.isProfile( "dev" ) ) {
			Map<String, String> hibernateProperties = new HashMap<>();
			hibernateProperties.put( "hibernate.show_sql", "false" );
			hibernateProperties.put( "hibernate.format_sql", "false" );
			hibernateProperties.put( "hibernate.use_sql_comments", "false" );
			properties.put( "acrossHibernate.hibernate-properties", hibernateProperties );
		}

	}

	@Override
	public void buildModuleStructure( ProjectGenerator generator, Map<String, Object> baseModel, ModulePaths modulePaths ) {
		ProjectRequest projectRequest = getProjectRequest();

		Map<String, Object> model = new LinkedHashMap<>( baseModel );
		model.put( "includeSampleEntity", projectRequest.containsOption( "ahm-sample-entity" ) );
		model.put( "hasEntityModule", projectRequest.containsModule( "entity-module" ) );
		generator.processTemplate( "modules/across-hibernate-jpa-module/EntityScanConfiguration.java", model,
		                           modulePaths.code( "extensions/EntityScanConfiguration.java" ) );

		if ( projectRequest.containsOption( "ahm-sample-entity" ) ) {
			if (isVersionBelow(AcrossVersions.PLATFORM_5_1_0)) {
				generator.processTemplate("modules/across-hibernate-jpa-module/DomainConfiguration.java", model,
						modulePaths.code("config/DomainConfiguration.java"));
			} else {
				generator.processTemplate("ax-5-1-0/modules/across-hibernate-jpa-module/DomainConfiguration.java", model,
						modulePaths.code("config/DomainConfiguration.java"));
			}

			// domain
			generator.processTemplate("modules/across-hibernate-jpa-module/BlogDomain.java", model,
					modulePaths.code("domain/blog/BlogDomain.java"));
			generator.processTemplate("modules/across-hibernate-jpa-module/Author.java", model,
					modulePaths.code("domain/blog/author/Author.java"));
			generator.processTemplate("modules/across-hibernate-jpa-module/AuthorRepository.java", model,
					modulePaths.code("domain/blog/author/AuthorRepository.java"));
			generator.processTemplate("modules/across-hibernate-jpa-module/BlogPost.java", model,
					modulePaths.code("domain/blog/post/BlogPost.java"));
			generator.processTemplate( "modules/across-hibernate-jpa-module/BlogPostRepository.java", model,
			                           modulePaths.code( "domain/blog/post/BlogPostRepository.java" ) );
			generator.processTemplate( "modules/across-hibernate-jpa-module/PublicationSettings.java", model,
			                           modulePaths.code( "domain/blog/post/PublicationSettings.java" ) );

			// installers
			generator.processTemplate( "modules/across-hibernate-jpa-module/SchemaInstaller.java", model,
			                           modulePaths.code( "installers/SchemaInstaller.java" ) );
			generator.processTemplate( "modules/across-hibernate-jpa-module/AuditableTablesInstaller.java", model,
			                           modulePaths.code( "installers/AuditableTablesInstaller.java" ) );
			generator.processTemplate( "modules/across-hibernate-jpa-module/SampleDataInstaller.java", model,
			                           modulePaths.code( "installers/SampleDataInstaller.java" ) );

			generator.copyResource(
					"modules/across-hibernate-jpa-module/schema.xml",
					modulePaths.resource( "installers/" + projectRequest.getApplicationResourcesKey() + "/schema/main.xml" )
			);

		}
	}
}
