package across.initializr.application.domain.generators.foreach;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;
import across.initializr.application.domain.project.ProjectGenerator;

import java.util.Collection;
import java.util.Map;

/**
 * @author Arne Vandamme
 */
public class ImageServerAdminWebGenerator extends GenericModuleGenerator
{
	@Override
	public void addApplicationClassImports( Collection<String> imports ) {
		imports.add( "org.springframework.context.annotation.Bean" );
		imports.add( "com.foreach.imageserver.admin.ImageServerAdminWebModule" );
	}

	@Override
	public void addApplicationClassCode( Collection<String> codeSnippets, ProjectGenerator generator, Map<String, Object> baseModel ) {
		codeSnippets.add( "@Bean\n" +
				                  "\tpublic ImageServerAdminWebModule imageServerAdminWebModule() {\n" +
				                  "\t\treturn new ImageServerAdminWebModule();\n" +
				                  "\t}" );
	}

	@Override
	public void addAcrossApplicationModules( Collection<String> modules ) {
	}

	@Override
	public void writeApplicationProperties( ApplicationProperties properties ) {
		if ( properties.isDefault() ) {
			properties.put( "imageServerAdmin.imageServerUrl", "/resources/images" );
			properties.put( "imageServerAdmin.accessToken", getProjectRequest().getApplicationResourcesKey() + "-imgserver-changeme" );
		}
	}
}
