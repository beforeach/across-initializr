package across.initializr.application.domain.project.web;

import across.initializr.application.domain.metadata.InitializrMetadata;
import across.initializr.application.domain.metadata.InitializrProperties;
import across.initializr.application.domain.metadata.PresetMetadata;
import across.initializr.application.domain.project.ProjectGenerator;
import across.initializr.application.domain.project.ProjectRequest;
import across.initializr.application.domain.project.ProjectRequestBuilder;
import across.initializr.application.domain.project.ProjectRequestForm;
import across.initializr.application.utils.Version;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.Set;

/**
 * @author Arne Vandamme
 */
@Controller
@RequiredArgsConstructor
@Slf4j
public class ProjectRequestController {

    private final InitializrProperties acrossInitializrProperties;
    private final AutowireCapableBeanFactory beanFactory;
    private final InitializrMetadata metadata;
    private final ProjectRequestBuilder projectRequestBuilder;

    @ModelAttribute("metadata")
    public InitializrMetadata metadata() {
        return metadata;
    }

    @GetMapping("/")
    public String renderProjectRequestForm(@ModelAttribute ProjectRequestForm projectRequest) {
        projectRequest.setPresetId( metadata.getPresets().get( 0 ).getId() );
        metadata.getPlatformVersions().stream()
                .filter( version -> !"snapshot".equalsIgnoreCase( version.getQualifier().getQualifier() ) )
                .findFirst().ifPresent( projectRequest::setPlatformVersion );
        return "th/acrossInitializr/generate-project";
    }

    @SuppressWarnings("unused")
    @SneakyThrows
    @PostMapping("/")
    public Object buildProject(@Valid @ModelAttribute ProjectRequestForm projectRequestForm, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            ProjectRequest projectRequest = projectRequestBuilder.buildProjectRequest( projectRequestForm );
            PresetMetadata preset = metadata.getPresetMetadata( projectRequestForm.getPresetId() );
            ProjectGenerator projectGenerator = beanFactory.createBean( preset.getGenerator() );

            String contentDispositionValue = "attachment; filename=\"" + projectRequest.getArtifact() + ".zip" + "\"";
            submitToInformationManager( projectRequestForm );
            return ResponseEntity.ok().header( "Content-Type", "application/zip" )
                    .header( "Content-Disposition", contentDispositionValue )
                    .body( projectGenerator.buildProject( projectRequest ) );
        }

        return renderProjectRequestForm( projectRequestForm );
    }


    private void submitToInformationManager(ProjectRequestForm form) {
        if (acrossInitializrProperties.getInformationManagerEndpoint() != null) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                FormDto dto = new FormDto()
                        .platformVersion(form.getPlatformVersion())
                        .modules(form.getSelectedModules());
                restTemplate.postForEntity(acrossInitializrProperties.getInformationManagerEndpoint(), dto, String.class);
            } catch (Exception e) {
                LOG.error("Error occurred whilst submitting the download to the information manager", e);
            }
        }
    }

    @Getter
    @Setter
    static class FormDto implements Serializable {
        private String platformVersion;
        private Set<String> modules;

        public FormDto platformVersion(Version platformVersion) {
            this.platformVersion = platformVersion.toString();
            return this;
        }

        public FormDto modules(Set<String> modules) {
            this.modules = modules;
            return this;
        }
    }

}
