package across.initializr.application.domain.project;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import java.io.StringWriter;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * @author Arne Vandamme
 */
@RequiredArgsConstructor
public class ApplicationProperties
{
	private final String profile;

	private Map<String, Object> map = new TreeMap<>();

	@SuppressWarnings("unchecked")
	public void put( String propertyName, Object value ) {
		String[] keys = StringUtils.split( propertyName, '.' );

		Map<String, Object> props = map;
		for ( int i = 0; i < keys.length; i++ ) {
			if ( i == keys.length - 1 ) {
				props.put( keys[i], value );
			}
			else {
				props = (TreeMap<String, Object>) props.computeIfAbsent( keys[i], ( k ) -> new TreeMap<String, Object>() );
			}
		}
	}

	public boolean isDefault() {
		return StringUtils.isEmpty( profile );
	}

	public boolean isProfile( String profile ) {
		return StringUtils.equalsIgnoreCase( this.profile, profile );
	}

	@SneakyThrows
	public String toYaml() {
		try (StringWriter sw = new StringWriter()) {
			writeMap( sw, map, 0 );
			return sw.toString();
		}
	}

	@SuppressWarnings("unchecked")
	private void writeMap( StringWriter output, Map<String, Object> map, int level ) {
		map.forEach( ( key, value ) -> {
			for ( int i = 0; i < level; i++ ) {
				output.write( "  " );
			}

			if ( value instanceof Map ) {
				output.write( key );
				output.write( ':' );
				output.write( '\n' );
				writeMap( output, (Map<String, Object>) value, level + 1 );
			}
			else {
				output.write( key );
				if ( !key.startsWith( "#" ) || value != null ) {
					output.write( ": " );

					String v = Objects.toString( value );
					output.write( StringUtils.contains( v, ':' ) ? '"' + v + '"' : v );
				}
				output.write( '\n' );
			}

			if ( level == 0 ) {
				output.write( '\n' );
			}
		} );
	}

}
