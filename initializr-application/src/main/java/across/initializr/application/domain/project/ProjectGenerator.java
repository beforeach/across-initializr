package across.initializr.application.domain.project;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.types.ZipFileSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StreamUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Arne Vandamme
 */
public abstract class ProjectGenerator
{
	private Path tempDir;
	private String id;
	private File dir;

	private ApplicationContext applicationContext;
	private TemplateRenderer templateRenderer;

	@SneakyThrows
	public final synchronized byte[] buildProject( ProjectRequest projectRequest ) {
		id = UUID.randomUUID().toString();
		tempDir = Paths.get( System.getProperty( "java.io.tmpdir" ) );

        if (!Files.exists(tempDir)) {
            Files.createDirectories(tempDir);
		}

		dir = Files.createTempDirectory( tempDir, id ).toFile();
        Files.createDirectories(dir.toPath());

		// Generate files
		generateProjectStructure( projectRequest );

		// build zip file
		File download = createProjectDistribution();

		// return zip output
		byte[] output;
		try (FileInputStream in = new FileInputStream( download )) {
			output = StreamUtils.copyToByteArray( in );
		}

		// remove
		FileSystemUtils.deleteRecursively( dir );
		FileSystemUtils.deleteRecursively( download );

		return output;
	}

	protected void generateProjectStructure( ProjectRequest projectRequest ) {
		copyResource( "shared/gitignore", ".gitignore" );
		copyResource( "shared/lombok.config", "lombok.config" );
		// Maven wrapper
		copyResource("shared/mvnw", "mvnw");
		copyResource("shared/mvnw.cmd", "mvnw.cmd");
		copyResource("shared/.mvn/wrapper/maven-wrapper.jar", ".mvn/wrapper/maven-wrapper.jar");
		copyResource("shared/.mvn/wrapper/maven-wrapper.properties", ".mvn/wrapper/maven-wrapper.properties");
		// Gradle wrapper
		copyResource("shared/gradlew", "gradlew");
		copyResource("shared/gradlew.bat", "gradlew.bat");
		copyResource("shared/gradle/wrapper/gradle-wrapper.properties", "gradle/wrapper/gradle-wrapper.properties");
		copyResource("shared/gradle/wrapper/gradle-wrapper.jar", "gradle/wrapper/gradle-wrapper.jar");
	}

	@SneakyThrows
	public final void copyResource( String templatePath, String fileName ) {
		try (InputStream in = applicationContext.getResource( "classpath:templates/" + templatePath ).getInputStream()) {
			try (OutputStream stream = new FileOutputStream( createFile( fileName ) )) {
				StreamUtils.copy( in, stream );
			}
		}
	}

	@SneakyThrows
	private File createProjectDistribution() {
		File download = new File( tempDir.toFile(), id + ".zip" );

		Zip zip = new Zip();
		zip.setProject( new Project() );
		zip.setDefaultexcludes( false );
		ZipFileSet set = new ZipFileSet();
		set.setDir( dir );
		set.setFileMode( "755" );
		set.setDefaultexcludes( false );
        zip.addFileset(set);
		zip.setDestFile( download.getCanonicalFile() );
		zip.execute();

		return download;
	}

	public final void processTemplate( String templateName, Map<String, Object> model, String target ) {
		writeText( createFile( target ), renderTemplate( templateName, model ) );
	}

	public final String renderTemplate( String templateFile, Map<String, Object> model ) {
		return templateRenderer.process( templateFile, model );
	}

	@SneakyThrows
	public final File createFile( String fileName ) {
		File file = new File( dir, fileName );
		FileUtils.forceMkdirParent( file );
		return file;
	}

	@SneakyThrows
	public final File createDirectory( String directoryName ) {
		File file = new File( dir, directoryName );
		FileUtils.forceMkdir(file);
		return file;
	}

    protected final void writeText(File target, String body) {
		try (OutputStream stream = new FileOutputStream( target )) {
			StreamUtils.copy( body, Charset.forName( "UTF-8" ), stream );
		}
		catch ( Exception e ) {
			throw new IllegalStateException( "Cannot write file " + target, e );
		}
	}

	public final void writeBinary( File target, byte[] body ) {
		try (OutputStream stream = new FileOutputStream( target )) {
			StreamUtils.copy( body, stream );
		}
		catch ( Exception e ) {
			throw new IllegalStateException( "Cannot write file " + target, e );
		}
	}

	@SneakyThrows
	public final void unzip( String zipFile, File folder ) {
		byte[] buffer = new byte[1024];

		//get the zip file content
		try (ZipInputStream zis = new ZipInputStream( applicationContext.getResource( "classpath:templates/" + zipFile ).getInputStream() )) {
			//get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while ( ze != null ) {

				String fileName = ze.getName();
				File newFile = new File( folder, fileName );

				if ( ze.isDirectory() ) {
					FileUtils.forceMkdir(newFile);
				}
				else {
					//create all non exists folders
					//else you will hit FileNotFoundException for compressed folder

					FileUtils.forceMkdir( new File( newFile.getParent() ) );

					try (FileOutputStream fos = new FileOutputStream( newFile )) {
						int len;
						while ( ( len = zis.read( buffer ) ) > 0 ) {
							fos.write( buffer, 0, len );
						}
					}
				}

				ze = zis.getNextEntry();
			}

			zis.closeEntry();
		}
	}

	@Autowired
	void setApplicationContext( ApplicationContext applicationContext ) {
		this.applicationContext = applicationContext;
	}

	@Autowired
	void setTemplateRenderer( TemplateRenderer templateRenderer ) {
		this.templateRenderer = templateRenderer;
	}
}
