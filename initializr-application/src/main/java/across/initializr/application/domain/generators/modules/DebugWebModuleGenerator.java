package across.initializr.application.domain.generators.modules;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;

import java.util.UUID;

/**
 * @author Arne Vandamme
 */
public class DebugWebModuleGenerator extends GenericModuleGenerator {
	@Override
	public void writeApplicationProperties(ApplicationProperties properties) {
		if (properties.isDefault()) {
			properties.put("debugWebModule.security.password", "'{noop}" + UUID.randomUUID() + "'");
			properties.put("debugWebModule.root-path", "/debug");
		}
	}
}
