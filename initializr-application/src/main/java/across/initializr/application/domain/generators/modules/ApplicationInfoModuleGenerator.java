package across.initializr.application.domain.generators.modules;

import across.initializr.application.domain.generators.GenericModuleGenerator;
import across.initializr.application.domain.project.ApplicationProperties;

/**
 * @author Arne Vandamme
 */
public class ApplicationInfoModuleGenerator extends GenericModuleGenerator
{
	@Override
	public void writeApplicationProperties( ApplicationProperties properties ) {
		if ( properties.isDefault() ) {
			properties.put( "applicationInfo.applicationId", getProjectRequest().getApplicationResourcesKey() );
			properties.put( "applicationInfo.applicationName", getProjectRequest().getApplicationName() );
		}
		else if ( properties.isProfile( "dev" ) ) {
			properties.put( "applicationInfo.environmentId", "development" );
		}
		else if ( properties.isProfile( "prod" ) ) {
			properties.put( "applicationInfo.environmentId", "production" );
		}
	}
}
