package {{packageName}};

import com.foreach.across.AcrossApplicationRunner;
import com.foreach.across.config.AcrossApplication;
{{#imports}}
import {{.}};
{{/imports}}

@AcrossApplication(
		modules = {
				{{moduleNames}}
		}
)
{{#applicationAnnotations}}{{.}}
{{/applicationAnnotations}}public class {{className}}
{
{{#codeSnippets}}
	{{.}}

{{/codeSnippets}}
	public static void main( String[] args ) {
		AcrossApplicationRunner.run( {{className}}.class, args );
	}
}
