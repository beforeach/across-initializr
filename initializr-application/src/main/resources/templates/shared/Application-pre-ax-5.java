package {{packageName}};

import com.foreach.across.config.AcrossApplication;
{{#imports}}
import {{.}};
{{/imports}}
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.h2.H2ConsoleAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.util.Collections;

@AcrossApplication(
		modules = {
				{{moduleNames}}
		}
)
{{#applicationAnnotations}}{{.}}
{{/applicationAnnotations}}public class {{className}}
{
{{#codeSnippets}}
	{{.}}

{{/codeSnippets}}
	public static void main( String[] args ) {
		SpringApplication.run( {{className}}.class, args );
	}
}