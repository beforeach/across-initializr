### Front-end resources
The source of the front-end resources is in `src/main/frontend`.
To configure resource processing, ensure you to `yarn` in the folder.
If yarn is not installed, perform `npm install yarn` first.