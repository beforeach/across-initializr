applicationInfo:
  applicationId: across-initializr
  applicationName: Across Initializr
  build-id: 2018-03-15

adminWebModule:
  login:
    rememberMe:
      key: ax-initializr

debugWebModule:
  security:
    password: '{noop}ax-debug'
  root-path: /ax-debug

initializr:
  templates:
    - name: Simple Web Application
      default-modules:
        - admin-web-module
    #- name: Multi-module Web Application
    #- name: Shared Module
    #- name: Standard Module
  platformVersions:
    - 5.2.0-SNAPSHOT
    - 5.1.0.RELEASE
    - 5.0.2.RELEASE
    - 2.1.5.RELEASE
  #    - 2.1.4.RELEASE
  #    - 2.1.3.RELEASE
  #    - 2.0.2.RELEASE
  #    - 2.0.1.RELEASE
  springBootConfigs:
    - id: spring-boot-starter-actuator
      name: Spring Boot Starter Actuator
      description: Provides actuator endpoints to help you monitor and manage your application.
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-amqp
      name: Spring Boot Starter AMQP
      description: Starter for using Spring AMQP and Rabbit MQ
      versionRange: 2.1.0.RELEASE
    #   Is included by default
#    - id: spring-boot-starter-jdbc
#      name: Spring Boot Starter JDBC
#      description: Spring Boot JDBC Starter
#      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-mail
      name: Spring Boot Starter Mail
      description: Starter for using Java Mail and Spring Framework's email sending support
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-websocket
      name: Spring Boot Starter Websocket
      description: Starter for building WebSocket applications using Spring Framework's WebSocket support
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-jpa
      name: Spring Boot Starter Data JPA
      description: Starter for using Spring Data JPA with Hibernate
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-cassandra
      name: Spring Boot Starter Data Cassandra
      description: Starter for using Cassandra distributed database and Spring Data Cassandra
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-couchbase
      name: Spring Boot Starter Data Couchbase
      description: Starter for using Couchbase document-oriented database and Spring Data Couchbase
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-elasticsearch
      name: Spring Boot Starter Data Elasticsearch
      description: Starter for using Elasticsearch search and analytics engine and Spring Data Elasticsearch
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-ldap
      name: Spring Boot Starter Data LDAP
      description: Starter for using Spring Data LDAP
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-mongodb
      name: Spring Boot Starter Data MongoDB
      description: Starter for using MongoDB document-oriented database and Spring Data MongoDB
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-neo4j
      name: Spring Boot Starter Data Neo4j
      description: Starter for using Neo4j graph database and Spring Data Neo4j
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-redis
      name: Spring Boot Starter Data Redis
      description: Starter for using Redis key-value data store with Spring Data Redis and the Lettuce client
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-rest
      name: Spring Boot Starter Data REST
      description: Starter for exposing Spring Data repositories over REST using Spring Data REST
      versionRange: 2.1.0.RELEASE
    - id: spring-boot-starter-data-solr
      name: Spring Boot Starter Data SOLR
      description: Starter for using the Apache Solr search platform with Spring Data Solr
      versionRange: 2.1.0.RELEASE
  presets:
    - id: single-blank
      name: Blank Across application without any modules
      generator: across.initializr.application.domain.generators.project.SingleApplicationGenerator
    - id: single-simple
      name: Basic Across application with low-level modules
      generator: across.initializr.application.domain.generators.project.SingleApplicationGenerator
      modules:
        - across-web-module
        - debug-web-module
        - application-info-module
        - logging-module
        - general-run-configuration
    - id: single-crud
      name: "CRUD application"
      description: Configuration for a CRUD application, includes user management, entity module configuration and custom admin controller samples.
      generator: across.initializr.application.domain.generators.project.SingleApplicationGenerator
      modules:
        - across-web-module
        - admin-web-module
        - debug-web-module
        - application-info-module
        - logging-module
        - user-module
        - across-hibernate-jpa-module
        - entity-module
        - general-message-source
        #- general-frontend-resources
        #- general-run-configuration
      options:
        - across-web-module
        - awm-admin-controller
        - ahm-sample-entity
    - id: single-website-custom
      name: "Website with custom backend"
      description: Configuration for a website without content management.  Sets up the administration UI and includes frontend layout and sample controllers.
      generator: across.initializr.application.domain.generators.project.SingleApplicationGenerator
      modules:
        - across-web-module
        - admin-web-module
        - debug-web-module
        - application-info-module
        - logging-module
        - user-module
        - across-hibernate-jpa-module
        - entity-module
        - general-message-source
        - general-sample-controllers
        #- general-run-configuration
      options:
        - awm-admin-controller
        - ahm-sample-entity
    - id: single-website-wcm
      name: "Website with basic content management"
      description: Configuration for a front-end website built on WebCmsModule.  Sets up the administration UI and provides an example WCM configuration.
      generator: across.initializr.application.domain.generators.project.SingleApplicationGenerator
      modules:
        - across-web-module
        - admin-web-module
        - debug-web-module
        - application-info-module
        - logging-module
        - user-module
        - across-hibernate-jpa-module
        - entity-module
        - general-message-source
        - general-sample-controllers
        #- general-frontend-resources
        #- general-run-configuration
        - web-cms-module
      options:
        - awm-admin-controller
        - ahm-sample-entity
        - wcm-samples
  module-groups:
    - name: Standard modules
      modules:
        - id: across-web-module
          name: AcrossWebModule
          packageName: com.foreach.across.modules.web
          description: Enables Spring MVC with Thymeleaf support for modules.  Adds Across web features like layout templates, web resources and menu building.
          groupId: com.foreach.across
          artifactId: across-web
          generator: across.initializr.application.domain.generators.GenericModuleGenerator
        - id: admin-web-module
          name: AdminWebModule
          packageName: com.foreach.across.modules.adminweb
          description: Provides basic infrastructure for creating an administration UI.
          groupId: com.foreach.across.modules
          artifactId: admin-web-module
          generator: across.initializr.application.domain.generators.modules.AdminWebModuleGenerator
          links:
            - rel: website
              href: https://foreach.atlassian.net/wiki/display/AX/AdminWebModule
            - rel: documentation
              href: http://across.foreach.be/docs/across-standard-modules/AdminWebModule/
              description: Reference and Javadoc
          options:
            - id: awm-admin-controller
              name: Include a sample admin controller
          dependencies:
            - across-web-module
        - id: debug-web-module
          name: DebugWebModule
          packageName: com.foreach.across.modules.debugweb
          description: Provides support for @DebugWebControllers in other modules. Provides facilities for introspecting a running AcrossContext configuration.
          groupId: com.foreach.across.modules
          artifactId: debug-web-module
          generator: across.initializr.application.domain.generators.modules.DebugWebModuleGenerator
          dependencies:
            - across-web-module
        - id: logging-module
          name: LoggingModule
          packageName: com.foreach.across.modules.logging
          description: Provides logging facilities for modules along with debug controllers to configure them.
          groupId: com.foreach.across.modules
          artifactId: logging-module
          generator: across.initializr.application.domain.generators.modules.LoggingModuleGenerator
        - id: application-info-module
          name: ApplicationInfoModule
          packageName: com.foreach.across.modules.applicationinfo
          description: Provides contextual information about the running application (eg. start time, build number).
          groupId: com.foreach.across.modules
          artifactId: application-info-module
          generator: across.initializr.application.domain.generators.modules.ApplicationInfoModuleGenerator
        - id: across-hibernate-jpa-module
          name: AcrossHibernateJpaModule
          packageName: com.foreach.across.modules.hibernate.jpa
          description: Provides JPA and custom Spring Data JPA support with Hibernate.
          groupId: com.foreach.across.modules
          artifactId: across-hibernate-module
          generator: across.initializr.application.domain.generators.modules.AcrossHibernateJpaModuleGenerator
          options:
            - id: ahm-sample-entity
              name: Include example blog entities with schema and data installers
        - id: user-module
          name: UserModule
          packageName: com.foreach.across.modules.user
          description: Provides infrastructure for managing users, groups and roles.
          groupId: com.foreach.across.modules
          artifactId: user-module
          generator: across.initializr.application.domain.generators.modules.UserModuleGenerator
          dependencies:
            - across-hibernate-jpa-module
        - id: entity-module
          name: EntityModule
          packageName: com.foreach.across.modules.entity
          description: Provides infrastructure for automatic generation of an administration UI for Spring Data managed entities.
          groupId: com.foreach.across.modules
          artifactId: entity-module
          dependencies:
            - across-web-module
        - id: web-cms-module
          name: WebCmsModule
          packageName: com.foreach.across.modules.webcms
          description: Provides infrastructure for Web Content Management and frontend rendering.
          groupId: com.foreach.across.modules
          artifactId : web-cms-module
          generator: across.initializr.application.domain.generators.modules.WebCmsModuleGenerator
          options:
            - id: wcm-samples
              name: Include sample pages and articles
            - id: wcm-imageserver
              name: Connect to a remote ImageServer for image management
            - id: wcm-cloudinary
              name: Connect to Cloudinary for image management
          dependencies:
            - across-hibernate-jpa-module
            - across-web-module
        - id: dynamic-forms-module
          name: DynamicFormsModule
          packageName: com.foreach.across.modules.dynamicforms
          description: Provides infrastructure for defining and managing custom documents and forms.
          groupId: com.foreach.across.modules
          artifactId: dynamic-forms-module
          dependencies:
            - entity-module
            - across-hibernate-jpa-module
          versionRange: 2.1.1.RELEASE
        - id: properties-module
          name: PropertiesModule
          packageName: com.foreach.across.modules.properties
          description: Allows to extend entities with typed properties.
          groupId: com.foreach.across.modules
          artifactId: properties-module
        - id: file-manager-module
          name: FileManagerModule
          packageName: com.foreach.across.modules.filemanager
          description: Provides an abstraction layer for storing and retrieving files. Allows to easily link files to entities when EntityModule, AcrossHibernateJpaModule and PropertiesModule are present.
          groupId: com.foreach.across.modules
          artifactId: file-manager-module
          generator: across.initializr.application.domain.generators.modules.FileManagerModuleGenerator
          dependencies:
          - across-hibernate-jpa-module
          - properties-module
        - id: ax-bootstrap-theme
          name: AdminWebThemesModule
          packageName: com.foreach.across.modules.adminwebthemes
          description: Configures the default Bootstrap 4 theme. If selected, additional styling will be added for the administrative section on top of the bootstrap 4 markup.
          groupId: com.foreach.across.modules
          artifactId: ax-bootstrap-theme
          dependencies:
            - admin-web-module
          versionRange: 5.0.0.RELEASE
          generator: across.initializr.application.domain.generators.modules.AdminWebThemesModuleGenerator

      # filemanager
        # oauth2module
        # springsecurityaclmodule
        # ldap module
        # spring batch
        # spring mobile
    - name: General configuration options
      modules:
        - id: general-message-source
          name: Include a default message source
          generator: across.initializr.application.domain.generators.general.MessageSourceGenerator
        - id: general-sample-controllers
          name: Include a frontend layout and controllers
          description: Adds a simple Start Bootstrap design implemented using Thymeleaf templates.  Depending on your module selection, you will end up having either a static or fully functional blog application.
          generator: across.initializr.application.domain.generators.general.SampleControllersGenerator
#        - id: general-frontend-resources
#          name: Front-end resources
#          description: Add an opinionated development structure for front-end resources like Gulp, Webpack...
#          generator: across.initializr.application.domain.generators.general.FrontendResourcesGenerator
#        - id: general-run-configuration
#          name: IntelliJ Run Configuration
#          description: Add a shared IntelliJ Run Configuration for development mode.  Requires you to open the project as an existing project and reimport maven sources instead of creating a new project from existing sources.
#          generator: across.initializr.application.domain.generators.general.IntelliJRunConfigurationGenerator
    - name: Contributed modules
      modules:
        - id: image-server-core-module
          name: ImageServerCoreModule
          description: Core module for running an ImageServer.
          groupId: com.foreach.imageserver
          packageName: com.foreach.imageserver.core
          artifactId: imageserver-core
          generator: across.initializr.application.domain.generators.foreach.ImageServerCoreGenerator
          dependencies:
            - across-hibernate-jpa-module
            - across-web-module
        - id: image-server-admin-web-module
          name: ImageServerAdminWebModule
          description: Administration interface for ImageServer.
          packageName: com.foreach.imageserver.admin
          groupId: com.foreach.imageserver
          artifactId: imageserver-admin
          generator: across.initializr.application.domain.generators.foreach.ImageServerAdminWebGenerator
          dependencies:
            - admin-web-module
            - user-module
            - image-server-core-module


# Processed by maven - leave as is
build.number: "@build.revision@"
applicationInfo.buildId: "@pom.version@-@build.revision@"
applicationInfo.buildDate: "@timestamp@"