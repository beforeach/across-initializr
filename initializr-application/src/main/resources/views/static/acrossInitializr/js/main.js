$( document ).ready( function () {
    $( 'input[type=radio]' ).on( 'click', function () {
        $( 'input[name=selectedModules]' ).each( function () {
            $( this ).prop( 'checked', false ).trigger( 'change' );
        } );
        var modules = $( this ).data( 'modules' );
        if ( modules ) {
            jQuery.each( modules.split( ',' ), function ( ix, moduleName ) {
                $( 'input[value="' + moduleName + '"]' ).prop( 'checked', true ).trigger( 'change' );
            } );
        }

        $( 'input[name=selectedOptions]' ).each( function () {
            $( this ).prop( 'checked', false ).trigger( 'change' );
        } );
        var options = $( this ).data( 'options' );
        if ( options ) {
            jQuery.each( options.split( ',' ), function ( ix, optionName ) {
                $( 'input[value="' + optionName + '"]' ).prop( 'checked', true ).trigger( 'change' );
            } );
        }

    } );

    $( '#applicationClassPrefix' ).on( 'input change', function () {
        var applicationClass = $( this ).val();
        var resourcesKey = applicationClass[0].toLowerCase() + applicationClass.substr( 1 );
        $( '#resourcesKey' ).val( resourcesKey );
    } ).on( 'keypress', function ( e ) {
        $( this ).data( 'changed', true );
        if ( e.which === 32 ) {
            return false;
        }
    } );
    $( '#packageName' ).on( 'keydown', function ( e ) {
        $( this ).data( 'changed', true );
        if ( e.which === 32 ) {
            return false;
        }
    } );
    $( '#applicationName' ).on( 'input change', function () {
        var className = $( '#applicationClassPrefix' );
        if ( !className.data( 'changed' ) ) {
            className.val( $( this ).val().replace( /\s/g, "" ) );
            className.trigger( 'change' );
        }
    } );
    $( '#group,#artifact' ).on( 'input change', function () {
        var pkg = $( '#packageName' );
        if ( !pkg.data( 'changed' ) ) {
            pkg.val( $( '#group' ).val() + '.' + $( '#artifact' ).val().replace( /-/g, "." ) );
        }
    } ).on( 'keypress', function ( e ) {
        if ( e.which === 32 ) {
            return false;
        }
    } );

    $( 'input' ).on( 'keypress', function ( event ) {
        if ( event.keyCode === 10 || event.keyCode === 13 ) {
            event.preventDefault();
        }
    } );

    $( '.module input' ).each( function () {
        var deps = $( this ).data( 'dependencies' );
        var versions = $( this ).data( 'versions' );
        if ( deps || versions ) {
            var cfg = {};
            if ( deps ) {
                var modules = deps.split( ',' );
                for ( var i = 0; i < modules.length; i++ ) {
                    var moduleId = '#' + modules[i];
                    cfg[moduleId] = {checked: true, enabled: true};
                }
            }
            if ( versions ) {
                cfg['#platformVersion'] = {
                    values: versions.split( ',' )
                };
            }
            $( this ).dependsOn( cfg, {hide: false} );
        }
    } );

    $( '.spring-boot-starter input' ).each( function () {
        var deps = $( this ).data( 'versions' );
        if ( deps ) {
            var cfg = {
                '#platformVersion': {
                    values: deps.split( ',' )
                }
            };
            $( this ).dependsOn( cfg, {hide: false} );
        }
    } );

    $( '.module-option input' ).each( function () {
        var module = '#' + $( this ).data( 'module' );
        var cfg = {};
        cfg[module] = {checked: true, enabled: true};
        $( this ).dependsOn( cfg, {hide: false} );
    } );
} );