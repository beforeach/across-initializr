package across.initializr.application.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestVersionParser {
    @Test
    public void versionParserCanReadOurSnapshots() {
        Version version = Version.from("2.1.0-SNAPSHOT");
        assertNotNull(version);
        assertEquals(2L, version.getMajor().longValue());
        assertEquals(1L, version.getMinor().longValue());
        assertEquals(0L, version.getPatch().longValue());
        assertEquals("SNAPSHOT", version.getQualifier().getQualifier());
        assertEquals("2.1.0-SNAPSHOT", version.toString());
    }

    @Test
    public void versionRangerParserWorksCorrectlyWithOurSnapshots() {
        VersionRange higherThan = VersionParser.DEFAULT.parseRange("2.0.1");
        assertTrue(higherThan.match(Version.from("2.1.0-SNAPSHOT")));
        assertFalse(higherThan.match(Version.from("2.0.0.RELEASE")));
        assertTrue(higherThan.match(Version.from("2.0.1.RELEASE")));
        assertTrue(higherThan.match(Version.from("2.0.2.RELEAE")));
    }
}
