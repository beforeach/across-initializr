package across.initializr;

import across.initializr.application.domain.metadata.InitializrMetadata;
import across.initializr.application.domain.project.ProjectRequestForm;
import across.initializr.application.domain.project.web.ProjectRequestController;
import across.initializr.application.utils.Version;
import com.foreach.across.core.context.registry.AcrossContextBeanRegistry;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.MemberValuePair;
import liquibase.util.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BindingResult;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = AcrossInitializrApplication.class)
public class TestAcrossInitializrApplication {
    private Version v2_1_0 = Version.from("2.1.0-SNAPSHOT");

    @Autowired
    private ProjectRequestController controller;
    @Autowired
    private AcrossContextBeanRegistry registry;

    private InitializrMetadata metadata;

    @Before
    public void setup() {
        metadata = registry.getBeanOfTypeFromModule("AcrossInitializrApplicationModule", InitializrMetadata.class);
    }

    @Test
    public void crudPresetAddsModules() throws IOException {
        for (Version version : metadata.getPlatformVersions()) {
            verifyFiles(projectRequestForm("single-crud", version, Collections.emptySet()), doc -> {
                Elements elements = doc.select("project").select("dependencies").select("artifactId");
                assertThat(elements).extracting(Element::text)
                        .contains("across-web", "admin-web-module", "logging-module", "application-info-module", "across-hibernate-module",
                                "debug-web-module", "user-module", "entity-module");
                assertThat(elements).extracting(Element::text).doesNotContain("web-cms-module");
            }, compilationUnit -> {
                Optional<ClassOrInterfaceDeclaration> c = compilationUnit.getClassByName("DemoApplication");
                c.ifPresent(clazz -> {
                    clazz.getAnnotationByName("AcrossApplication").ifPresent(ax -> {
                        assertThat(
                                ((MemberValuePair) ax.getChildNodes().get(1)).getValue().toString()
                        ).isEqualTo("{ DebugWebModule.NAME, UserModule.NAME, LoggingModule.NAME, AdminWebModule.NAME, ApplicationInfoModule.NAME, AcrossHibernateJpaModule.NAME, EntityModule.NAME, AcrossWebModule.NAME }");
                    });
                });
            });
        }
    }

    @Test
    public void allVersionsLessThan2_1_0_ShouldNeverAllowAutoConfiguration() throws IOException {
        for (Version version : metadata.getPlatformVersions()) {
            verifyFiles(projectRequestForm("single-blank", version, Collections.emptySet()), doc -> {
                if (version.compareTo(v2_1_0) >= 0) {
                    assertThat(doc.select("project").select("dependencies").select("artifactId")).extracting(Element::text).contains("across-autoconfigure");
                } else {
                    assertThat(doc.select("project").select("dependencies").select("artifactId")).extracting(Element::text).doesNotContain("across-autoconfigure");
                }
            }, null);
        }
    }

    @Test
    public void noAdditionalSpringBootConfigsGeneratedWhenNotSelected() throws IOException {
        verifyFiles(projectRequestForm("single-blank", Version.from("2.1.0-SNAPSHOT"), Collections.emptySet()), doc -> {
            List<Element> starters = doc.select("project").select("dependencies").select("artifactId").stream()
                    .filter(e -> StringUtils.startsWith(e.text(), "spring-boot-starter")).collect(Collectors.toList());
            assertThat(starters).extracting(Element::text).hasSize(3).contains("spring-boot-starter-web", "spring-boot-starter-jdbc", "spring-boot-starter-validation");
        }, null);
    }

    @Test
    public void additionalSpringBootConfigsGeneratedWhenSelected() throws IOException {
        String[] additionalSpringBootConfigs = new String[]{"spring-boot-starter-actuator", "spring-boot-starter-amqp", /*"spring-boot-starter-jdbc",*/
                "spring-boot-starter-mail", "spring-boot-starter-websocket", "spring-boot-starter-data-jpa", "spring-boot-starter-data-cassandra", "spring-boot-starter-data-couchbase", "spring-boot-starter-data-elasticsearch", "spring-boot-starter-data-ldap", "spring-boot-starter-data-mongodb", "spring-boot-starter-data-neo4j", "spring-boot-starter-data-redis"
        };
        verifyFiles(projectRequestForm("single-blank", Version.from("2.1.0-SNAPSHOT"), Arrays.stream(additionalSpringBootConfigs).collect(Collectors.toSet())), doc -> {
            List<Element> starters = doc.select("project").select("dependencies").select("artifactId").stream()
                    .filter(e -> StringUtils.startsWith(e.text(), "spring-boot-starter")).collect(Collectors.toList());
            assertThat(starters).extracting(Element::text).hasSize(3 + additionalSpringBootConfigs.length)
                    .contains("spring-boot-starter-web", "spring-boot-starter-jdbc", "spring-boot-starter-validation")
                    .contains(additionalSpringBootConfigs);
        }, null);
    }

    private void verifyFiles(ProjectRequestForm projectRequestForm, Consumer<Document> pomConsumer, Consumer<CompilationUnit> javaFileConsumer) throws IOException {
        byte[] bytes = generateProject(projectRequestForm);
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes)) {
            try (ZipInputStream zis = new ZipInputStream(byteArrayInputStream)) {
                ZipEntry entry;
                while ((entry = zis.getNextEntry()) != null) {
                    String output = stringFromZipInputStream(zis);
                    if (javaFileConsumer != null && "java".equals(FilenameUtils.getExtension(entry.getName()))) {
                        javaFileConsumer.accept(JavaParser.parse(output));
                    }

                    switch (entry.getName()) {
                        case "pom.xml":
                            Document doc = Jsoup.parse(output);
                            assertNotNull(doc);
                            pomConsumer.accept(doc);
                            break;
                        // You may add other consumers here
                    }
                }
            }
        }
    }

    private byte[] generateProject(ProjectRequestForm projectRequestForm) {
        ResponseEntity response = (ResponseEntity) controller.buildProject(projectRequestForm, mock(BindingResult.class));
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        byte[] bytes = (byte[]) response.getBody();
        assertNotNull(bytes);
        return bytes;
    }

    private ProjectRequestForm projectRequestForm(String presetId, Version version, Set<String> additionalSpringBootConfigs) {
        ProjectRequestForm projectRequestForm = new ProjectRequestForm();
        projectRequestForm.setPresetId(presetId);
        projectRequestForm.setSelectedModules(new HashSet<>(metadata.getPresetMetadata(presetId).getModules()));
        projectRequestForm.setSelectedSpringBootConfigs(additionalSpringBootConfigs);
        projectRequestForm.setPlatformVersion(version);
        return projectRequestForm;
    }

    private String stringFromZipInputStream(ZipInputStream zis) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        IOUtils.copy(zis, byteArrayOutputStream);
        String s = byteArrayOutputStream.toString();
        assertNotNull(s);
        return s;
    }

}
